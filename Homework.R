

# Answer num 1 (Done)
Matrix <- matrix(1:12, nrow =4, ncol = 3)
rownames(Matrix) <- c("Q", "W", "E", "R")
colnames(Matrix) <- c("A", "B", "C")
print(Matrix)



# Answer num 2 (Done)
x <- 24
y <- "Hello World"
z <- 93.65
class(x)
class(y)
class(z)
Numbers_factor <- c(x, y, z)
trimws(Numbers_factor)
paste(Numbers_factor)


# Answer num 3 (Done)
q = 65.9836
#Part A
signif(q, 3)
#Part B
Q <- log10(q)
  if (Q < 2)
  print("The answer is less than 2")


# Answer num 4 (Done)
x <- c("Intelligence", "Knowledge", "Wisdom", "Comprehension")
y = "I am"
z = "intelligent"
#Part A
Num4A <- substring(x, 1, 4)
print(Num4A)
#Part B
Sentence <- c(y ,z)
print(Sentence)
#Part C
Answer <- c(x ,y, z)
New_answer <- toupper(Answer)
print(New_answer)


# Answer num 5 (Done)
a <- c(3, 4, 14, 17, 3, 98, 66, 85, 44)
format(a)
for(i in a)
  ifelse(i%%3==0, print("yes"), print("no"))
  
  
  
# Answer num 6 (Done)
b <- c(36,3,5,19,2,16,18,41,35,28,30,31)
format(b)
for (i in b)
  if (i < 30){
    print(i)
}


# Answer num 7 (Done)
#Part A
Date = ("01/30/18")
Date_new <- as.Date(Date, format="%m-%d-%y")
.print(Date_new)
#Part B
months(Date_new)
weekdays(Date_new)
#Part C
Sysdate <- Sys.Date()
Sysdate - Date_new



